/*
 * Copyright (C) 2024 UBports Foundation
 *
 * Authors:
 *    Muhammad <muhammad23012009@hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "usb-config-helper.h"

UsbConfigHelper::UsbConfigHelper(QObject *parent):
  QObject(parent)
{
    m_iface = new QDBusInterface(USB_MODED_SERVICE, USB_MODED_PATH, USB_MODED_SERVICE, QDBusConnection::systemBus(), this);

    if (m_iface->isValid()) {
        QDBusConnection::systemBus().connect(USB_MODED_SERVICE, USB_MODED_PATH, USB_MODED_SERVICE, "sig_usb_target_state_ind", this, SLOT(targetModeChanged(QString)));

        QDBusReply<QString> reply = m_iface->call("get_config");
        updateValues(reply.value());
    }
}

UsbConfigHelper::ConfigMode UsbConfigHelper::str_to_mode(QString str)
{
    if (str.contains("charging_only"))
        return USB_CHARGING_ONLY;
    else if (str.contains("mtp"))
        return USB_MTP;
    else if (str.contains("rndis"))
        return USB_RNDIS;
}

QString UsbConfigHelper::mode_to_str(ConfigMode mode)
{
    if (mode == USB_CHARGING_ONLY)
        return "charging_only";
    else if (mode == USB_MTP)
        return "mtp";
    else if (mode == USB_RNDIS)
        return "rndis";
}

// public
bool UsbConfigHelper::developerModeCapable()
{
    if (m_iface->isValid()) {
        QDBusReply<QString> reply = m_iface->call("get_modes");
        return reply.value().contains("_adb");
    }

    return false;
}

UsbConfigHelper::ConfigMode UsbConfigHelper::getMode()
{
    return m_mode;
}

void UsbConfigHelper::setMode(ConfigMode mode)
{
    if (m_mode == mode || m_mode == USB_INVALID)
        return;

    m_mode = mode;
    QString mode_str = mode_to_str(mode);

    if (m_adbEnabled)
        mode_str.append("_adb");

    setMode(mode_str);

    Q_EMIT modeChanged();
}

bool UsbConfigHelper::getAdbEnabled()
{
    return m_adbEnabled;
}

void UsbConfigHelper::setAdbEnabled(bool enabled)
{
    if (m_adbEnabled == enabled)
        return;

    m_adbEnabled = enabled;
    QString mode_str = mode_to_str(m_mode);
    if (m_adbEnabled)
        mode_str.append("_adb");

    setMode(mode_str);

    Q_EMIT adbEnabledChanged();
}

void UsbConfigHelper::setMode(QString str)
{
    m_iface->call("set_config", str);
    m_iface->call("set_mode", str);
}

void UsbConfigHelper::targetModeChanged(QString mode)
{
    updateValues(mode);
}

void UsbConfigHelper::updateValues(QString mode)
{
    if (str_to_mode(mode) != m_mode) {
        m_mode = str_to_mode(mode);
        Q_EMIT modeChanged();
    }

    m_adbEnabled = mode.contains("_adb");

    Q_EMIT adbEnabledChanged();
}
